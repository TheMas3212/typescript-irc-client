import typescript from 'rollup-plugin-typescript2';
import pkg from './package.json';
import builtins from 'rollup-plugin-node-builtins';
import globals from 'rollup-plugin-node-globals';

export default [
  {
    input: 'src/index.ts',
    output: {
      file: pkg.main,
      format: 'cjs',
    },
    external: [
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {}),
      'tls',
      'net',
      'events'
    ],
    plugins: [
      typescript({
        typescript: require('typescript'),
      }),
    ],
  },
  {
    input: 'src/index.es.ts',
    output: {
      file: pkg.module,
      format: 'es',
    },
    external: [
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {}),
      'ws'
    ],
    plugins: [
      typescript({
        typescript: require('typescript'),
      }),
      globals(),
      builtins()
    ],
  }
]

