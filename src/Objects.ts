class Prefix {
    realname: string;
    username: string;
    hostname: string;
    constructor(raw: string) {
        this.realname = this.username = this.hostname = "";
        if (raw.charAt(0) === ":") {
            let raw2 = raw.slice(1).split(/[!@]/);
            this.realname = raw2[0];
            this.username = raw2[1];
            this.hostname = raw2[2];
        }
    }
}
class Channel {
    name: string;
    join_state: boolean;
    constructor(name: string) {
        this.name = name;
        this.join_state = false;
    }
    joinCallback() {
        this.join_state = true;
    }
}
class Message {
    raw: string;
    tags: {};
    command: string;
    prefix: Prefix;
    parameters: string[];
    constructor(raw: string) {
        this.raw = raw;
        this.tags = {};
        this.parameters = [];
        let data = raw.split(" ");
        let data2 = data.shift();
        let a = data2.charAt(0);
        if (a === "@") {
            data2.slice(1).split(";").forEach((r_tag) => {
                let s_tag = r_tag.split("=", 2);
                this.tags[s_tag[0]] = s_tag[1];
            });
            data2 = data.shift();
            a = data2.charAt(0);
        }
        if (a === ":") {
            this.prefix = new Prefix(data2);
            data2 = data.shift();
            a = data2.charAt(0);
        }
        this.command = data2;
        for (; ;) {
            if (data[0] == undefined) {
                break;
            }
            else if (data[0].charAt(0) === ":") {
                let parameter = data.join(" ").slice(1);
                this.parameters.push(parameter);
                break;
            }
            else if (data.length === 1) {
                let parameter = data[0];
                this.parameters.push(parameter);
                break;
            }
            else {
                let parameter = data.shift();
                this.parameters.push(parameter);
            }
        }
    }
    get channel() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE':
            case 'HOSTTARGET':
            case 'ROOMSTATE':
            case 'USERNOTICE':
            case 'USERSTATE': {
                return this.parameters[0];
            }
            default: {
                return undefined;
            }
        }
    }
    get sender() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.prefix.realname;
            }
            default: {
                return undefined;
            }
        }
    }
    get message() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.parameters[1];
            }
            default: {
                return undefined;
            }
        }
    }
}
class TwitchMessage extends Message {
    constructor(data: string) {
        super(data);
    }
    get bits() {
        const bits = this.tags['bits'];
        return parseInt(bits) || 0;
    }
    get isModerator() {
        return this.tags['badges'] && (this.tags['badges'].includes('moderator') || this.isBroadcaster);
    }
    get isBroadcaster() {
        return this.tags['badges'] && (this.tags['badges'].includes('broadcaster'));
    }
    get isSubscriber() {
        return this.tags['badges'] && (this.tags['badges'].includes('subscriber'));
    }
    get isTurbo() {
        return this.tags['badges'] && (this.tags['badges'].includes('turbo'));
    }
    get isPrime() {
        return this.tags['badges'] && (this.tags['badges'].includes('premium'));
    }
    get isPartner() {
        return this.tags['badges'] && (this.tags['badges'].includes('partner'));
    }
    get isVIP() {
        return this.tags['badges'] && (this.tags['badges'].includes('vip'));
    }
    get isAdmin() {
        return this.tags['badges'] && (this.tags['badges'].includes('admin'));
    }
    get isGlobalMod() {
        return this.tags['badges'] && (this.tags['badges'].includes('global_mod'));
    }
    get isStaff() {
        return this.tags['badges'] && (this.tags['badges'].includes('staff'));
    }
    // TODO gettings for:
    // bits-charity
    // bits-leader
    // bits
    get color() {
        return this.tags['color'];
    }
}
export { Prefix, Message, TwitchMessage, Channel };