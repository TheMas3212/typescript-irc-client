import { EventEmitter } from 'events';
import { Channel } from './Objects';
import { WebSocketClient } from './WebSocketClientB';
import { ClientInterface } from './BaseClient';

export * from './Objects';
export { TwitchClientWS } from './WebSocketClientB';
export { ClientState } from './BaseClient';

export type ClientMode = 'ws';
export type WebSocketOptions = {
    "mode": "ws",
    "url": string
}
export type SharedOptions = {
    "nickname": string,
    "logging"?: boolean,
    "preConnectCommands"?: string[],
    "postConnectCommands"?: string[]
}
export type ClientOptions = (WebSocketOptions) & SharedOptions

export class Client implements ClientInterface {
    options: ClientOptions
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: WebSocket;
    constructor(options: ClientOptions) {
        switch(options.mode || 'ws') {
            case ('ws'): {
                return new WebSocketClient(options as WebSocketOptions & SharedOptions)
            }
            default:
        }
    }
    connect(){};
    joinChannel(channel: string): boolean {return false};
    sendData(data: string, log?: boolean): void {};
    sendDataRaw(data: Buffer, log?: boolean): void {};
    findChannel(channel: string): Channel {return new Channel('')};
    leaveChannel(channel: string): void {};
    listChannels(): void {};
    disconnect(): void {};
    reconnect(): void {};
    on(eventName: string, listener: Function): EventEmitter {return new EventEmitter};
    off(eventName: string, listener: Function): EventEmitter {return new EventEmitter};
    once(eventName: string, listener: Function): EventEmitter {return new EventEmitter};
}