import { EventEmitter } from 'events';
import { Channel } from './Objects';
import { WebSocketClient } from './WebSocketClientN';
import { SSLClient, NETClient } from './SocketClient';
import { ClientInterface } from './BaseClient';
import { WebSocketOptions, SocketOptions, SharedOptions } from './Types';
import * as TLS from 'tls';
import * as NET from 'net';

export * from './Objects';
export * from './Types';
export { TwitchClient } from './SocketClient';
export { TwitchClientWS } from './WebSocketClientN';
export { ClientState } from './BaseClient';

export type ClientMode = 'tls' | 'net' | 'ws';
export type ClientOptions = (WebSocketOptions | SocketOptions) & SharedOptions

export class Client implements ClientInterface {
    options: ClientOptions
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: NET.Socket | TLS.TLSSocket | WebSocket;
    constructor(options: ClientOptions) {
        switch(options.mode || 'tls') {
            case ('tls'): {
                return new SSLClient(options as SocketOptions & SharedOptions);
            }
            case ('net'): {
                return new NETClient(options as SocketOptions & SharedOptions);
            }
            case ('ws'): {
                return new WebSocketClient(options as WebSocketOptions & SharedOptions);
            }
            default:
        }
    }
    connect(){};
    joinChannel(channel: string): boolean {return false;};
    sendData(data: string): void {};
    sendDataRaw(data: Buffer): void {};
    findChannel(channel: string): Channel {return new Channel('');};
    leaveChannel(channel: string): void {};
    listChannels(): void {};
    disconnect(): void {};
    reconnect(): void {};
    on(eventName: string, listener: Function): EventEmitter {return new EventEmitter;};
    off(eventName: string, listener: Function): EventEmitter {return new EventEmitter;};
    once(eventName: string, listener: Function): EventEmitter {return new EventEmitter;};
}