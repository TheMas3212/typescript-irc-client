import * as TLS from 'tls';
import * as NET from 'net';
import { Message, TwitchMessage, Channel } from './Objects';
import { BaseClient, ClientInterface, ClientState } from "./BaseClient";
import { SocketOptions, SharedOptions, TwitchOptions } from './Types';
export class SocketClient extends BaseClient implements ClientInterface {
    options: SocketOptions & SharedOptions;
    // port: string;
    // host: string;
    // nickname: string;
    // preConnectCommands: string[];
    // postConnectCommands: string[];
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: NET.Socket | TLS.TLSSocket;
    constructor(options: SocketOptions & SharedOptions) {
        super();
        this.options = options;
        this.options.preConnectCommands = options.preConnectCommands || [];
        this.options.postConnectCommands = options.postConnectCommands || [];
        this.channels = [];
        this.status = ClientState.Disconnected;
        this.hanging_data = "";
    }
    _connect_stage2() {
        this.initWatchdog();
        this.socket.setEncoding("utf8");
        this.socket.setNoDelay(true);
        this.socket.on("close", () => {
            if (this.status != ClientState.Quiting) {
                this.status = ClientState.Disconnected;
                this.channels.forEach((channel) => {
                    channel.join_state = false;
                });
                setTimeout(this.connect.bind(this), 5000);
            }
            else {
                this.status = ClientState.Quit;
            }
        });
        this.socket.on("data", (data: string) => {
            let newdata = this.hanging_data + data;
            let lines = newdata.split("\r\n");
            this.hanging_data = lines.pop();
            lines.forEach((data2) => {
                if (data2 != "") {
                    this.resetWatchdog();
                    let msg = this.processMessage(data2);
                    if (msg.command === "PING") {
                        this.socket.write(data2.replace("PING", "PONG")+'\r\n');
                    }
                    if (msg.command === "422" || msg.command === "376") {
                        this.status = ClientState.Ready;
                        this.options.postConnectCommands.forEach((cmd) => {
                            this.sendData(cmd);
                        });
                        this.channels.forEach((channel) => {
                            this.sendData("JOIN " + channel.name);
                        });
                    }
                    ;
                    if (msg.command === "JOIN") {
                        let chn_name = msg.parameters[0];
                        let chn = this.findChannel(chn_name);
                        if (chn === null) {
                            chn = new Channel(chn_name);
                            this.channels.push(chn);
                        }
                        chn.joinCallback();
                    }
                    ;
                    this.emit('message', msg);
                }
            });
        });
        this.socket.on("error", (error) => {
            console.log("ERROR", error);
        });
    }
    sendData(data: string, log: boolean = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.write(data + "\r\n");
    }
    sendDataRaw(data: Buffer, log: boolean = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.write(Buffer.concat([data, Buffer.from("\r\n")]));
    }
    reconnect() {
        this.disconnect();
        setTimeout(this.connect.bind(this), 3000);
    }
    _disconnect() {
        try {
            this.socket.end();
            this.socket.destroy();
        } catch (err) {
            console.error(err);
        }
    }
}
export class SSLClient extends SocketClient {
    socket: TLS.TLSSocket;
    constructor(options: SocketOptions & SharedOptions) {
        super(options);
    }
    _connect_callback() {
        if (!this.socket.authorized) {
            this.reconnect();
            // throw "SECURE SOCKET FAILURE";
        }
        else {
            console.log("Port:", this.socket.remotePort);
            console.log("Host:", this.socket.remoteAddress);
            console.log("Connection Authorized:", this.socket.authorized);
            this.status = ClientState.Connected;
            this.options.preConnectCommands.forEach((cmd) => {
                this.sendData(cmd);
            });
            this.sendData("NICK " + this.options.nickname);
            this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
        }
    }
    connect() {
        if (this.status === ClientState.Connected) {
            return;
        }
        else {
            let options = {
                host: this.options.host,
                port: parseInt(this.options.port),
                servername: this.options.host
            };
            this.socket = TLS.connect(options);
            this.socket.on("secureConnect", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
}
export class NETClient extends SocketClient {
    socket: NET.Socket;
    constructor(options: SocketOptions & SharedOptions) {
        super(options);
    }
    _connect_callback() {
        console.log("Port:", this.socket.remotePort);
        console.log("Host:", this.socket.remoteAddress);
        this.status = ClientState.Connected;
        this.options.preConnectCommands.forEach((cmd) => {
            this.sendData(cmd);
        });
        this.sendData("NICK " + this.options.nickname);
        this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
    }
    connect() {
        if (this.status === ClientState.Connected) {
            return;
        }
        else {
            let options = {
                host: this.options.host,
                port: parseInt(this.options.port),
                servername: this.options.host
            };
            this.socket = NET.connect(options);
            this.socket.on("connect", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
}
export class TwitchClient extends SSLClient {
    constructor(options: TwitchOptions & SharedOptions) {
        const newOptions: SocketOptions & SharedOptions = {
            ...options,
            "mode": "tls",
            "host": "irc.chat.twitch.tv",
            "port": "6697"
        };
        newOptions.preConnectCommands = [ ...options.preConnectCommands || [],
            "CAP REQ :twitch.tv/tags",
            "CAP REQ :twitch.tv/commands",
            "CAP REQ :twitch.tv/membership"
        ];
        if (!options.nickname || !options.oauth) {
            newOptions.nickname = 'justinfan' + (Math.floor(Math.random() * (900000)) + 100000).toString();
        }
        else {
            newOptions.preConnectCommands.push("PASS " + options.oauth);
        }
        super(newOptions);
        this.on('message', this.twitch_join_handler.bind(this));
    }
    twitch_join_handler(msg: Message) {
        if (msg.command === "ROOMSTATE") {
            let roomname = "#" + msg.parameters[0];
            this.channels.forEach((channel) => {
                if (channel.name === roomname) {
                    channel.join_state = true;
                }
            });
        }
        else if (msg.command === "RECONNECT") {
            setTimeout(this.reconnect.bind(this), 2000);
        }
    }
    processMessage(data: string) {
        return new TwitchMessage(data);
    }
}
