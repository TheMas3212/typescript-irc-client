import { Channel, TwitchMessage } from './Objects';
import { BaseClient, ClientInterface, ClientState } from "./BaseClient";
import { WebSocketOptions, SharedOptions, TwitchOptions } from './Types';
const WebSocket = require('ws');

export class WebSocketClient extends BaseClient implements ClientInterface {
    options: WebSocketOptions & SharedOptions;
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: WebSocket;
    constructor(options: WebSocketOptions & SharedOptions) {
        super();
        this.options = options;
        this.options.preConnectCommands = options.preConnectCommands || [];
        this.options.postConnectCommands = options.postConnectCommands || [];
        this.channels = [];
        this.status = ClientState.Disconnected;
        this.hanging_data = "";
    }
    _connect_callback() {
        console.log("URL:", this.socket.url);
        this.status = ClientState.Connected;
        this.options.preConnectCommands.forEach((cmd) => {
            this.sendData(cmd);
        });
        this.sendData("NICK " + this.options.nickname);
        this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
    }
    connect() {
        if (this.status === ClientState.Connected) {
            return;
        }
        else {
            this.socket = new WebSocket(this.options.url);
            this.socket.addEventListener("open", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
    _connect_stage2() {
        this.initWatchdog();
        this.socket.addEventListener("close", () => {
            if (this.status != ClientState.Quiting) {
                this.status = ClientState.Disconnected;
                this.channels.forEach((channel) => {
                    channel.join_state = false;
                });
                setTimeout(this.connect.bind(this), 5000);
            }
            else {
                this.status = ClientState.Quit;
            }
        });
        this.socket.addEventListener("message", (event) => {
            let newdata = this.hanging_data + event.data;
            let lines = newdata.split("\r\n");
            this.hanging_data = lines.pop();
            lines.forEach((data2) => {
                if (data2 != "") {
                    this.resetWatchdog();
                    let msg = this.processMessage(data2);
                    if (msg.command === "PING") {
                        this.socket.send(data2.replace("PING", "PONG")+'\r\n');
                    }
                    if (msg.command === "422" || msg.command === "376") {
                        this.status = ClientState.Ready;
                        this.options.postConnectCommands.forEach((cmd) => {
                            this.sendData(cmd);
                        });
                        this.channels.forEach((channel) => {
                            this.sendData("JOIN " + channel.name);
                        });
                    }
                    ;
                    if (msg.command === "JOIN") {
                        let chn_name = msg.parameters[0];
                        let chn = this.findChannel(chn_name);
                        if (chn === null) {
                            chn = new Channel(chn_name);
                            this.channels.push(chn);
                        }
                        chn.joinCallback();
                    }
                    ;
                    this.emit('message', msg);
                }
            });
        });
        this.socket.addEventListener("error", (error) => {
            console.log("ERROR", error);
        });
    }
    _disconnect() {
        try {
            this.socket.close();
        } catch (err) {
            console.error(err);
        }
    }
    reconnect() {
        this.disconnect();
        setTimeout(this.connect.bind(this), 3000);
    }
    sendData(data: string, log: boolean = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(data + "\r\n");
    }
    sendDataRaw(data: Buffer, log: boolean = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(Buffer.concat([data, Buffer.from("\r\n")]));
    }
}
export class TwitchClientWS extends WebSocketClient {
    options: TwitchOptions & WebSocketOptions & SharedOptions;
    constructor(options: TwitchOptions & WebSocketOptions & SharedOptions) {
        const newOptions: TwitchOptions & WebSocketOptions & SharedOptions = {
            ...options,
            "mode": "ws",
            "url": "wss://irc-ws.chat.twitch.tv:443/"
        };
        newOptions.preConnectCommands = [ ...options.preConnectCommands || [],
            "CAP REQ :twitch.tv/tags",
            "CAP REQ :twitch.tv/commands",
            "CAP REQ :twitch.tv/membership"
        ];
        if (!options.nickname || !options.oauth) {
            newOptions.nickname = 'justinfan' + (Math.floor(Math.random() * (900000)) + 100000).toString();
        }
        else {
            newOptions.preConnectCommands.push("PASS " + options.oauth);
        }
        super(newOptions);
        this.on('message', this.twitch_join_handler.bind(this));
    }
    twitch_join_handler(msg: TwitchMessage) {
        if (msg.command === "ROOMSTATE") {
            let roomname = "#" + msg.parameters[0];
            this.channels.forEach((channel) => {
                if (channel.name === roomname) {
                    channel.join_state = true;
                }
            });
        }
        else if (msg.command === "RECONNECT") {
            setTimeout(this.reconnect.bind(this), 2000);
        }
    }
    processMessage(data: string) {
        return new TwitchMessage(data);
    }
}
