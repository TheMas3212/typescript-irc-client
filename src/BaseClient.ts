import { EventEmitter } from 'events';
import { Message, Channel } from './Objects';
export enum ClientState {
    Disconnected,
    Connected,
    Ready,
    Quiting,
    Quit
}

const watchdogTimeout: number = 1000*60*7;

export interface ClientInterface {
    connect(): void;
    joinChannel(channel: string): boolean;
    sendData(data: string, log: boolean): void;
    sendDataRaw(data: Buffer, log: boolean): void;
    findChannel(channel: string): Channel;
    joinChannel(channel: string): boolean;
    leaveChannel(channel: string): void;
    listChannels(): void;
    disconnect(): void;
    reconnect(): void;
    on(eventName: string, listener: Function): EventEmitter;
    off(eventName: string, listener: Function): EventEmitter;
    once(eventName: string, listener: Function): EventEmitter;
}

export class BaseClient extends EventEmitter {
    channels: Channel[];
    status: ClientState;
    watchdog: NodeJS.Timeout
    constructor() {
        super();
    }
    initWatchdog() {
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    resetWatchdog() {
        clearTimeout(this.watchdog);
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    _watchdog() {
        clearTimeout(this.watchdog);
        this.reconnect();
    }
    connect() { }
    disconnect() {
        this.status = ClientState.Quiting;
        try {
            this.sendData("QUIT");
        } catch (err) {}
        setTimeout(this._disconnect.bind(this), 1000);
    }
    _disconnect() { }
    reconnect() { }
    sendData(data: string, log: boolean = true) { }
    processMessage(data: string) {
        return new Message(data);
    }
    findChannel(channel: string) {
        let found_chn = null;
        this.channels.forEach((chn) => {
            if (channel === chn.name) {
                found_chn = chn;
            }
        });
        return found_chn;
    }
    joinChannel(channel: string) {
        if (this.findChannel(channel) === null) {
            let chn = new Channel(channel);
            if (this.status === ClientState.Ready) {
                this.sendData("JOIN " + chn.name);
            }
            this.channels.push(chn);
            return true;
        }
        else {
            console.log("Already in channel:", channel);
            return false;
        }
    }
    leaveChannel(channel: string) {
        let chn = this.findChannel(channel);
        if (chn === null) {
            console.error("channel doesn't exist");
        }
        else {
            const index = this.channels.indexOf(chn);
            this.channels.splice(index, 1);
            this.sendData("PART " + chn.name);
        }
    }
    listChannels() {
        console.log("Channels:");
        this.channels.forEach((channel) => {
            console.log(channel.name, channel.join_state);
        });
    }
}
