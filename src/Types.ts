export type WebSocketOptions = {
    "mode": "ws",
    "url": string
};
export type SocketOptions = {
    "mode": "tls" | "net",
    "host": string,
    "port": string
};
export type SharedOptions = {
    "nickname": string,
    "logging"?: boolean,
    "preConnectCommands"?: string[],
    "postConnectCommands"?: string[]
};
export type TwitchOptions = {
  "oauth"?: string
};