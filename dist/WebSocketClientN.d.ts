/// <reference types="node" />
import { Channel, TwitchMessage } from './Objects';
import { BaseClient, ClientInterface } from "./BaseClient";
import { WebSocketOptions, SharedOptions, TwitchOptions } from './Types';
export declare class WebSocketClient extends BaseClient implements ClientInterface {
    options: WebSocketOptions & SharedOptions;
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: WebSocket;
    constructor(options: WebSocketOptions & SharedOptions);
    _connect_callback(): void;
    connect(): void;
    _connect_stage2(): void;
    _disconnect(): void;
    reconnect(): void;
    sendData(data: string, log?: boolean): void;
    sendDataRaw(data: Buffer, log?: boolean): void;
}
export declare class TwitchClientWS extends WebSocketClient {
    options: TwitchOptions & WebSocketOptions & SharedOptions;
    constructor(options: TwitchOptions & WebSocketOptions & SharedOptions);
    twitch_join_handler(msg: TwitchMessage): void;
    processMessage(data: string): TwitchMessage;
}
