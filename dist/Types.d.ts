export declare type WebSocketOptions = {
    "mode": "ws";
    "url": string;
};
export declare type SocketOptions = {
    "mode": "tls" | "net";
    "host": string;
    "port": string;
};
export declare type SharedOptions = {
    "nickname": string;
    "logging"?: boolean;
    "preConnectCommands"?: string[];
    "postConnectCommands"?: string[];
};
export declare type TwitchOptions = {
    "oauth"?: string;
};
