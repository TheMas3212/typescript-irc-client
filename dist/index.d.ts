/// <reference types="node" />
import { EventEmitter } from 'events';
import { Channel } from './Objects';
import { ClientInterface } from './BaseClient';
import { WebSocketOptions, SocketOptions, SharedOptions } from './Types';
import * as TLS from 'tls';
import * as NET from 'net';
export * from './Objects';
export * from './Types';
export { TwitchClient } from './SocketClient';
export { TwitchClientWS } from './WebSocketClientN';
export { ClientState } from './BaseClient';
export declare type ClientMode = 'tls' | 'net' | 'ws';
export declare type ClientOptions = (WebSocketOptions | SocketOptions) & SharedOptions;
export declare class Client implements ClientInterface {
    options: ClientOptions;
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: NET.Socket | TLS.TLSSocket | WebSocket;
    constructor(options: ClientOptions);
    connect(): void;
    joinChannel(channel: string): boolean;
    sendData(data: string): void;
    sendDataRaw(data: Buffer): void;
    findChannel(channel: string): Channel;
    leaveChannel(channel: string): void;
    listChannels(): void;
    disconnect(): void;
    reconnect(): void;
    on(eventName: string, listener: Function): EventEmitter;
    off(eventName: string, listener: Function): EventEmitter;
    once(eventName: string, listener: Function): EventEmitter;
}
