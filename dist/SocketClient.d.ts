/// <reference types="node" />
import * as TLS from 'tls';
import * as NET from 'net';
import { Message, TwitchMessage, Channel } from './Objects';
import { BaseClient, ClientInterface } from "./BaseClient";
import { SocketOptions, SharedOptions, TwitchOptions } from './Types';
export declare class SocketClient extends BaseClient implements ClientInterface {
    options: SocketOptions & SharedOptions;
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: NET.Socket | TLS.TLSSocket;
    constructor(options: SocketOptions & SharedOptions);
    _connect_stage2(): void;
    sendData(data: string, log?: boolean): void;
    sendDataRaw(data: Buffer, log?: boolean): void;
    reconnect(): void;
    _disconnect(): void;
}
export declare class SSLClient extends SocketClient {
    socket: TLS.TLSSocket;
    constructor(options: SocketOptions & SharedOptions);
    _connect_callback(): void;
    connect(): void;
}
export declare class NETClient extends SocketClient {
    socket: NET.Socket;
    constructor(options: SocketOptions & SharedOptions);
    _connect_callback(): void;
    connect(): void;
}
export declare class TwitchClient extends SSLClient {
    constructor(options: TwitchOptions & SharedOptions);
    twitch_join_handler(msg: Message): void;
    processMessage(data: string): TwitchMessage;
}
