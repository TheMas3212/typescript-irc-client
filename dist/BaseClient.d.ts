/// <reference types="node" />
import { EventEmitter } from 'events';
import { Message, Channel } from './Objects';
export declare enum ClientState {
    Disconnected = 0,
    Connected = 1,
    Ready = 2,
    Quiting = 3,
    Quit = 4
}
export interface ClientInterface {
    connect(): void;
    joinChannel(channel: string): boolean;
    sendData(data: string, log: boolean): void;
    sendDataRaw(data: Buffer, log: boolean): void;
    findChannel(channel: string): Channel;
    joinChannel(channel: string): boolean;
    leaveChannel(channel: string): void;
    listChannels(): void;
    disconnect(): void;
    reconnect(): void;
    on(eventName: string, listener: Function): EventEmitter;
    off(eventName: string, listener: Function): EventEmitter;
    once(eventName: string, listener: Function): EventEmitter;
}
export declare class BaseClient extends EventEmitter {
    channels: Channel[];
    status: ClientState;
    watchdog: NodeJS.Timeout;
    constructor();
    initWatchdog(): void;
    resetWatchdog(): void;
    _watchdog(): void;
    connect(): void;
    disconnect(): void;
    _disconnect(): void;
    reconnect(): void;
    sendData(data: string, log?: boolean): void;
    processMessage(data: string): Message;
    findChannel(channel: string): any;
    joinChannel(channel: string): boolean;
    leaveChannel(channel: string): void;
    listChannels(): void;
}
