var domain;

// This constructor is used to store event handlers. Instantiating this is
// faster than explicitly calling `Object.create(null)` to get a "clean" empty
// object (tested with v8 v4.9).
function EventHandlers() {}
EventHandlers.prototype = Object.create(null);

function EventEmitter() {
  EventEmitter.init.call(this);
}

// nodejs oddity
// require('events') === require('events').EventEmitter
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.usingDomains = false;

EventEmitter.prototype.domain = undefined;
EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

EventEmitter.init = function() {
  this.domain = null;
  if (EventEmitter.usingDomains) {
    // if there is an active domain, then attach to it.
    if (domain.active ) ;
  }

  if (!this._events || this._events === Object.getPrototypeOf(this)._events) {
    this._events = new EventHandlers();
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || isNaN(n))
    throw new TypeError('"n" argument must be a positive number');
  this._maxListeners = n;
  return this;
};

function $getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return $getMaxListeners(this);
};

// These standalone emit* functions are used to optimize calling of event
// handlers for fast cases because emit() itself often has a variable number of
// arguments and can be deoptimized because of that. These functions always have
// the same number of arguments and thus do not get deoptimized, so the code
// inside them can execute faster.
function emitNone(handler, isFn, self) {
  if (isFn)
    handler.call(self);
  else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      listeners[i].call(self);
  }
}
function emitOne(handler, isFn, self, arg1) {
  if (isFn)
    handler.call(self, arg1);
  else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      listeners[i].call(self, arg1);
  }
}
function emitTwo(handler, isFn, self, arg1, arg2) {
  if (isFn)
    handler.call(self, arg1, arg2);
  else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      listeners[i].call(self, arg1, arg2);
  }
}
function emitThree(handler, isFn, self, arg1, arg2, arg3) {
  if (isFn)
    handler.call(self, arg1, arg2, arg3);
  else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      listeners[i].call(self, arg1, arg2, arg3);
  }
}

function emitMany(handler, isFn, self, args) {
  if (isFn)
    handler.apply(self, args);
  else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      listeners[i].apply(self, args);
  }
}

EventEmitter.prototype.emit = function emit(type) {
  var er, handler, len, args, i, events, domain;
  var doError = (type === 'error');

  events = this._events;
  if (events)
    doError = (doError && events.error == null);
  else if (!doError)
    return false;

  domain = this.domain;

  // If there is no 'error' event listener then throw.
  if (doError) {
    er = arguments[1];
    if (domain) {
      if (!er)
        er = new Error('Uncaught, unspecified "error" event');
      er.domainEmitter = this;
      er.domain = domain;
      er.domainThrown = false;
      domain.emit('error', er);
    } else if (er instanceof Error) {
      throw er; // Unhandled 'error' event
    } else {
      // At least give some kind of context to the user
      var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
      err.context = er;
      throw err;
    }
    return false;
  }

  handler = events[type];

  if (!handler)
    return false;

  var isFn = typeof handler === 'function';
  len = arguments.length;
  switch (len) {
    // fast cases
    case 1:
      emitNone(handler, isFn, this);
      break;
    case 2:
      emitOne(handler, isFn, this, arguments[1]);
      break;
    case 3:
      emitTwo(handler, isFn, this, arguments[1], arguments[2]);
      break;
    case 4:
      emitThree(handler, isFn, this, arguments[1], arguments[2], arguments[3]);
      break;
    // slower
    default:
      args = new Array(len - 1);
      for (i = 1; i < len; i++)
        args[i - 1] = arguments[i];
      emitMany(handler, isFn, this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  if (typeof listener !== 'function')
    throw new TypeError('"listener" argument must be a function');

  events = target._events;
  if (!events) {
    events = target._events = new EventHandlers();
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (!existing) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] = prepend ? [listener, existing] :
                                          [existing, listener];
    } else {
      // If we've already got an array, just append.
      if (prepend) {
        existing.unshift(listener);
      } else {
        existing.push(listener);
      }
    }

    // Check for listener leak
    if (!existing.warned) {
      m = $getMaxListeners(target);
      if (m && m > 0 && existing.length > m) {
        existing.warned = true;
        var w = new Error('Possible EventEmitter memory leak detected. ' +
                            existing.length + ' ' + type + ' listeners added. ' +
                            'Use emitter.setMaxListeners() to increase limit');
        w.name = 'MaxListenersExceededWarning';
        w.emitter = target;
        w.type = type;
        w.count = existing.length;
        emitWarning(w);
      }
    }
  }

  return target;
}
function emitWarning(e) {
  typeof console.warn === 'function' ? console.warn(e) : console.log(e);
}
EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function _onceWrap(target, type, listener) {
  var fired = false;
  function g() {
    target.removeListener(type, g);
    if (!fired) {
      fired = true;
      listener.apply(target, arguments);
    }
  }
  g.listener = listener;
  return g;
}

EventEmitter.prototype.once = function once(type, listener) {
  if (typeof listener !== 'function')
    throw new TypeError('"listener" argument must be a function');
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      if (typeof listener !== 'function')
        throw new TypeError('"listener" argument must be a function');
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      if (typeof listener !== 'function')
        throw new TypeError('"listener" argument must be a function');

      events = this._events;
      if (!events)
        return this;

      list = events[type];
      if (!list)
        return this;

      if (list === listener || (list.listener && list.listener === listener)) {
        if (--this._eventsCount === 0)
          this._events = new EventHandlers();
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length; i-- > 0;) {
          if (list[i] === listener ||
              (list[i].listener && list[i].listener === listener)) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (list.length === 1) {
          list[0] = undefined;
          if (--this._eventsCount === 0) {
            this._events = new EventHandlers();
            return this;
          } else {
            delete events[type];
          }
        } else {
          spliceOne(list, position);
        }

        if (events.removeListener)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events;

      events = this._events;
      if (!events)
        return this;

      // not listening for removeListener, no need to emit
      if (!events.removeListener) {
        if (arguments.length === 0) {
          this._events = new EventHandlers();
          this._eventsCount = 0;
        } else if (events[type]) {
          if (--this._eventsCount === 0)
            this._events = new EventHandlers();
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        for (var i = 0, key; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = new EventHandlers();
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners) {
        // LIFO order
        do {
          this.removeListener(type, listeners[listeners.length - 1]);
        } while (listeners[0]);
      }

      return this;
    };

EventEmitter.prototype.listeners = function listeners(type) {
  var evlistener;
  var ret;
  var events = this._events;

  if (!events)
    ret = [];
  else {
    evlistener = events[type];
    if (!evlistener)
      ret = [];
    else if (typeof evlistener === 'function')
      ret = [evlistener.listener || evlistener];
    else
      ret = unwrapListeners(evlistener);
  }

  return ret;
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? Reflect.ownKeys(this._events) : [];
};

// About 1.5x faster than the two-arg version of Array#splice().
function spliceOne(list, index) {
  for (var i = index, k = i + 1, n = list.length; k < n; i += 1, k += 1)
    list[i] = list[k];
  list.pop();
}

function arrayClone(arr, i) {
  var copy = new Array(i);
  while (i--)
    copy[i] = arr[i];
  return copy;
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}

class Prefix {
    constructor(raw) {
        this.realname = this.username = this.hostname = "";
        if (raw.charAt(0) === ":") {
            let raw2 = raw.slice(1).split(/[!@]/);
            this.realname = raw2[0];
            this.username = raw2[1];
            this.hostname = raw2[2];
        }
    }
}
class Channel {
    constructor(name) {
        this.name = name;
        this.join_state = false;
    }
    joinCallback() {
        this.join_state = true;
    }
}
class Message {
    constructor(raw) {
        this.raw = raw;
        this.tags = {};
        this.parameters = [];
        let data = raw.split(" ");
        let data2 = data.shift();
        let a = data2.charAt(0);
        if (a === "@") {
            data2.slice(1).split(";").forEach((r_tag) => {
                let s_tag = r_tag.split("=", 2);
                this.tags[s_tag[0]] = s_tag[1];
            });
            data2 = data.shift();
            a = data2.charAt(0);
        }
        if (a === ":") {
            this.prefix = new Prefix(data2);
            data2 = data.shift();
            a = data2.charAt(0);
        }
        this.command = data2;
        for (;;) {
            if (data[0] == undefined) {
                break;
            }
            else if (data[0].charAt(0) === ":") {
                let parameter = data.join(" ").slice(1);
                this.parameters.push(parameter);
                break;
            }
            else if (data.length === 1) {
                let parameter = data[0];
                this.parameters.push(parameter);
                break;
            }
            else {
                let parameter = data.shift();
                this.parameters.push(parameter);
            }
        }
    }
    get channel() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE':
            case 'HOSTTARGET':
            case 'ROOMSTATE':
            case 'USERNOTICE':
            case 'USERSTATE': {
                return this.parameters[0];
            }
            default: {
                return undefined;
            }
        }
    }
    get sender() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.prefix.realname;
            }
            default: {
                return undefined;
            }
        }
    }
    get message() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.parameters[1];
            }
            default: {
                return undefined;
            }
        }
    }
}
class TwitchMessage extends Message {
    constructor(data) {
        super(data);
    }
    get bits() {
        const bits = this.tags['bits'];
        return parseInt(bits) || 0;
    }
    get isModerator() {
        return this.tags['badges'] && (this.tags['badges'].includes('moderator') || this.isBroadcaster);
    }
    get isBroadcaster() {
        return this.tags['badges'] && (this.tags['badges'].includes('broadcaster'));
    }
    get isSubscriber() {
        return this.tags['badges'] && (this.tags['badges'].includes('subscriber'));
    }
    get isTurbo() {
        return this.tags['badges'] && (this.tags['badges'].includes('turbo'));
    }
    get isPrime() {
        return this.tags['badges'] && (this.tags['badges'].includes('premium'));
    }
    get isPartner() {
        return this.tags['badges'] && (this.tags['badges'].includes('partner'));
    }
    get isVIP() {
        return this.tags['badges'] && (this.tags['badges'].includes('vip'));
    }
    get isAdmin() {
        return this.tags['badges'] && (this.tags['badges'].includes('admin'));
    }
    get isGlobalMod() {
        return this.tags['badges'] && (this.tags['badges'].includes('global_mod'));
    }
    get isStaff() {
        return this.tags['badges'] && (this.tags['badges'].includes('staff'));
    }
    get color() {
        return this.tags['color'];
    }
}

var ClientState;
(function (ClientState) {
    ClientState[ClientState["Disconnected"] = 0] = "Disconnected";
    ClientState[ClientState["Connected"] = 1] = "Connected";
    ClientState[ClientState["Ready"] = 2] = "Ready";
    ClientState[ClientState["Quiting"] = 3] = "Quiting";
    ClientState[ClientState["Quit"] = 4] = "Quit";
})(ClientState || (ClientState = {}));
const watchdogTimeout = 1000 * 60 * 7;
class BaseClient extends EventEmitter {
    constructor() {
        super();
    }
    initWatchdog() {
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    resetWatchdog() {
        clearTimeout(this.watchdog);
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    _watchdog() {
        clearTimeout(this.watchdog);
        this.reconnect();
    }
    connect() { }
    disconnect() {
        this.status = ClientState.Quiting;
        try {
            this.sendData("QUIT");
        }
        catch (err) { }
        setTimeout(this._disconnect.bind(this), 1000);
    }
    _disconnect() { }
    reconnect() { }
    sendData(data, log = true) { }
    processMessage(data) {
        return new Message(data);
    }
    findChannel(channel) {
        let found_chn = null;
        this.channels.forEach((chn) => {
            if (channel === chn.name) {
                found_chn = chn;
            }
        });
        return found_chn;
    }
    joinChannel(channel) {
        if (this.findChannel(channel) === null) {
            let chn = new Channel(channel);
            if (this.status === ClientState.Ready) {
                this.sendData("JOIN " + chn.name);
            }
            this.channels.push(chn);
            return true;
        }
        else {
            console.log("Already in channel:", channel);
            return false;
        }
    }
    leaveChannel(channel) {
        let chn = this.findChannel(channel);
        if (chn === null) {
            console.error("channel doesn't exist");
        }
        else {
            const index = this.channels.indexOf(chn);
            this.channels.splice(index, 1);
            this.sendData("PART " + chn.name);
        }
    }
    listChannels() {
        console.log("Channels:");
        this.channels.forEach((channel) => {
            console.log(channel.name, channel.join_state);
        });
    }
}

class WebSocketClient extends BaseClient {
    constructor(options) {
        super();
        this.options = options;
        this.options.preConnectCommands = options.preConnectCommands || [];
        this.options.postConnectCommands = options.postConnectCommands || [];
        this.channels = [];
        this.status = ClientState.Disconnected;
        this.hanging_data = "";
    }
    _connect_callback() {
        console.log("URL:", this.socket.url);
        this.status = ClientState.Connected;
        this.options.preConnectCommands.forEach((cmd) => {
            this.sendData(cmd);
        });
        this.sendData("NICK " + this.options.nickname);
        this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
    }
    connect() {
        if (this.status === ClientState.Connected) {
            return;
        }
        else {
            this.socket = new WebSocket(this.options.url);
            this.socket.addEventListener("open", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
    _connect_stage2() {
        this.initWatchdog();
        this.socket.addEventListener("close", () => {
            if (this.status != ClientState.Quiting) {
                this.status = ClientState.Disconnected;
                this.channels.forEach((channel) => {
                    channel.join_state = false;
                });
                setTimeout(this.connect.bind(this), 3000);
            }
            else {
                this.status = ClientState.Quit;
            }
        });
        this.socket.addEventListener("message", (event) => {
            let newdata = this.hanging_data + event.data;
            let lines = newdata.split("\r\n");
            this.hanging_data = lines.pop();
            lines.forEach((data2) => {
                if (data2 != "") {
                    this.resetWatchdog();
                    let msg = this.processMessage(data2);
                    if (msg.command === "PING") {
                        this.socket.send(data2.replace("PING", "PONG") + '\r\n');
                    }
                    if (msg.command === "422" || msg.command === "376") {
                        this.status = ClientState.Ready;
                        this.options.postConnectCommands.forEach((cmd) => {
                            this.sendData(cmd);
                        });
                        this.channels.forEach((channel) => {
                            this.sendData("JOIN " + channel.name);
                        });
                    }
                    if (msg.command === "JOIN") {
                        let chn_name = msg.parameters[0];
                        let chn = this.findChannel(chn_name);
                        if (chn === null) {
                            chn = new Channel(chn_name);
                            this.channels.push(chn);
                        }
                        chn.joinCallback();
                    }
                    this.emit('message', msg);
                }
            });
        });
        this.socket.addEventListener("error", (error) => {
            console.log("ERROR", error);
        });
    }
    _disconnect() {
        try {
            this.socket.close();
        }
        catch (err) {
            console.error(err);
        }
    }
    reconnect() {
        this.disconnect();
        setTimeout(this.connect.bind(this), 3000);
    }
    sendData(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(data + "\r\n");
    }
    sendDataRaw(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(Buffer.concat([data, Buffer.from("\r\n")]));
    }
}
class TwitchClientWS extends WebSocketClient {
    constructor(options) {
        const newOptions = Object.assign(Object.assign({}, options), { "mode": "ws", "url": "wss://irc-ws.chat.twitch.tv:443/" });
        newOptions.preConnectCommands = [...options.preConnectCommands || [],
            "CAP REQ :twitch.tv/tags",
            "CAP REQ :twitch.tv/commands",
            "CAP REQ :twitch.tv/membership"
        ];
        if (!options.nickname || !options.oauth) {
            newOptions.nickname = 'justinfan' + (Math.floor(Math.random() * (900000)) + 100000).toString();
        }
        else {
            newOptions.preConnectCommands.push("PASS " + options.oauth);
        }
        super(newOptions);
        this.on('message', this.twitch_join_handler.bind(this));
    }
    twitch_join_handler(msg) {
        if (msg.command === "ROOMSTATE") {
            let roomname = "#" + msg.parameters[0];
            this.channels.forEach((channel) => {
                if (channel.name === roomname) {
                    channel.join_state = true;
                }
            });
        }
        else if (msg.command === "RECONNECT") {
            setTimeout(this.reconnect.bind(this), 2000);
        }
    }
    processMessage(data) {
        return new TwitchMessage(data);
    }
}

class Client {
    constructor(options) {
        switch (options.mode || 'ws') {
            case ('ws'): {
                return new WebSocketClient(options);
            }
        }
    }
    connect() { }
    ;
    joinChannel(channel) { return false; }
    ;
    sendData(data, log) { }
    ;
    sendDataRaw(data, log) { }
    ;
    findChannel(channel) { return new Channel(''); }
    ;
    leaveChannel(channel) { }
    ;
    listChannels() { }
    ;
    disconnect() { }
    ;
    reconnect() { }
    ;
    on(eventName, listener) { return new EventEmitter; }
    ;
    off(eventName, listener) { return new EventEmitter; }
    ;
    once(eventName, listener) { return new EventEmitter; }
    ;
}

export { Channel, Client, ClientState, Message, Prefix, TwitchClientWS, TwitchMessage };
