declare class Prefix {
    realname: string;
    username: string;
    hostname: string;
    constructor(raw: string);
}
declare class Channel {
    name: string;
    join_state: boolean;
    constructor(name: string);
    joinCallback(): void;
}
declare class Message {
    raw: string;
    tags: {};
    command: string;
    prefix: Prefix;
    parameters: string[];
    constructor(raw: string);
    get channel(): string;
    get sender(): string;
    get message(): string;
}
declare class TwitchMessage extends Message {
    constructor(data: string);
    get bits(): number;
    get isModerator(): any;
    get isBroadcaster(): any;
    get isSubscriber(): any;
    get isTurbo(): any;
    get isPrime(): any;
    get isPartner(): any;
    get isVIP(): any;
    get isAdmin(): any;
    get isGlobalMod(): any;
    get isStaff(): any;
    get color(): any;
}
export { Prefix, Message, TwitchMessage, Channel };
