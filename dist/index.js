'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var events = require('events');
var TLS = require('tls');
var NET = require('net');

class Prefix {
    constructor(raw) {
        this.realname = this.username = this.hostname = "";
        if (raw.charAt(0) === ":") {
            let raw2 = raw.slice(1).split(/[!@]/);
            this.realname = raw2[0];
            this.username = raw2[1];
            this.hostname = raw2[2];
        }
    }
}
class Channel {
    constructor(name) {
        this.name = name;
        this.join_state = false;
    }
    joinCallback() {
        this.join_state = true;
    }
}
class Message {
    constructor(raw) {
        this.raw = raw;
        this.tags = {};
        this.parameters = [];
        let data = raw.split(" ");
        let data2 = data.shift();
        let a = data2.charAt(0);
        if (a === "@") {
            data2.slice(1).split(";").forEach((r_tag) => {
                let s_tag = r_tag.split("=", 2);
                this.tags[s_tag[0]] = s_tag[1];
            });
            data2 = data.shift();
            a = data2.charAt(0);
        }
        if (a === ":") {
            this.prefix = new Prefix(data2);
            data2 = data.shift();
            a = data2.charAt(0);
        }
        this.command = data2;
        for (;;) {
            if (data[0] == undefined) {
                break;
            }
            else if (data[0].charAt(0) === ":") {
                let parameter = data.join(" ").slice(1);
                this.parameters.push(parameter);
                break;
            }
            else if (data.length === 1) {
                let parameter = data[0];
                this.parameters.push(parameter);
                break;
            }
            else {
                let parameter = data.shift();
                this.parameters.push(parameter);
            }
        }
    }
    get channel() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE':
            case 'HOSTTARGET':
            case 'ROOMSTATE':
            case 'USERNOTICE':
            case 'USERSTATE': {
                return this.parameters[0];
            }
            default: {
                return undefined;
            }
        }
    }
    get sender() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.prefix.realname;
            }
            default: {
                return undefined;
            }
        }
    }
    get message() {
        switch (this.command) {
            case 'PRIVMSG':
            case 'NOTICE': {
                return this.parameters[1];
            }
            default: {
                return undefined;
            }
        }
    }
}
class TwitchMessage extends Message {
    constructor(data) {
        super(data);
    }
    get bits() {
        const bits = this.tags['bits'];
        return parseInt(bits) || 0;
    }
    get isModerator() {
        return this.tags['badges'] && (this.tags['badges'].includes('moderator') || this.isBroadcaster);
    }
    get isBroadcaster() {
        return this.tags['badges'] && (this.tags['badges'].includes('broadcaster'));
    }
    get isSubscriber() {
        return this.tags['badges'] && (this.tags['badges'].includes('subscriber'));
    }
    get isTurbo() {
        return this.tags['badges'] && (this.tags['badges'].includes('turbo'));
    }
    get isPrime() {
        return this.tags['badges'] && (this.tags['badges'].includes('premium'));
    }
    get isPartner() {
        return this.tags['badges'] && (this.tags['badges'].includes('partner'));
    }
    get isVIP() {
        return this.tags['badges'] && (this.tags['badges'].includes('vip'));
    }
    get isAdmin() {
        return this.tags['badges'] && (this.tags['badges'].includes('admin'));
    }
    get isGlobalMod() {
        return this.tags['badges'] && (this.tags['badges'].includes('global_mod'));
    }
    get isStaff() {
        return this.tags['badges'] && (this.tags['badges'].includes('staff'));
    }
    get color() {
        return this.tags['color'];
    }
}

(function (ClientState) {
    ClientState[ClientState["Disconnected"] = 0] = "Disconnected";
    ClientState[ClientState["Connected"] = 1] = "Connected";
    ClientState[ClientState["Ready"] = 2] = "Ready";
    ClientState[ClientState["Quiting"] = 3] = "Quiting";
    ClientState[ClientState["Quit"] = 4] = "Quit";
})(exports.ClientState || (exports.ClientState = {}));
const watchdogTimeout = 1000 * 60 * 7;
class BaseClient extends events.EventEmitter {
    constructor() {
        super();
    }
    initWatchdog() {
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    resetWatchdog() {
        clearTimeout(this.watchdog);
        this.watchdog = setTimeout(this._watchdog.bind(this), watchdogTimeout);
    }
    _watchdog() {
        clearTimeout(this.watchdog);
        this.reconnect();
    }
    connect() { }
    disconnect() {
        this.status = exports.ClientState.Quiting;
        try {
            this.sendData("QUIT");
        }
        catch (err) { }
        setTimeout(this._disconnect.bind(this), 1000);
    }
    _disconnect() { }
    reconnect() { }
    sendData(data, log = true) { }
    processMessage(data) {
        return new Message(data);
    }
    findChannel(channel) {
        let found_chn = null;
        this.channels.forEach((chn) => {
            if (channel === chn.name) {
                found_chn = chn;
            }
        });
        return found_chn;
    }
    joinChannel(channel) {
        if (this.findChannel(channel) === null) {
            let chn = new Channel(channel);
            if (this.status === exports.ClientState.Ready) {
                this.sendData("JOIN " + chn.name);
            }
            this.channels.push(chn);
            return true;
        }
        else {
            console.log("Already in channel:", channel);
            return false;
        }
    }
    leaveChannel(channel) {
        let chn = this.findChannel(channel);
        if (chn === null) {
            console.error("channel doesn't exist");
        }
        else {
            const index = this.channels.indexOf(chn);
            this.channels.splice(index, 1);
            this.sendData("PART " + chn.name);
        }
    }
    listChannels() {
        console.log("Channels:");
        this.channels.forEach((channel) => {
            console.log(channel.name, channel.join_state);
        });
    }
}

const WebSocket = require('ws');
class WebSocketClient extends BaseClient {
    constructor(options) {
        super();
        this.options = options;
        this.options.preConnectCommands = options.preConnectCommands || [];
        this.options.postConnectCommands = options.postConnectCommands || [];
        this.channels = [];
        this.status = exports.ClientState.Disconnected;
        this.hanging_data = "";
    }
    _connect_callback() {
        console.log("URL:", this.socket.url);
        this.status = exports.ClientState.Connected;
        this.options.preConnectCommands.forEach((cmd) => {
            this.sendData(cmd);
        });
        this.sendData("NICK " + this.options.nickname);
        this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
    }
    connect() {
        if (this.status === exports.ClientState.Connected) {
            return;
        }
        else {
            this.socket = new WebSocket(this.options.url);
            this.socket.addEventListener("open", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
    _connect_stage2() {
        this.initWatchdog();
        this.socket.addEventListener("close", () => {
            if (this.status != exports.ClientState.Quiting) {
                this.status = exports.ClientState.Disconnected;
                this.channels.forEach((channel) => {
                    channel.join_state = false;
                });
                setTimeout(this.connect.bind(this), 5000);
            }
            else {
                this.status = exports.ClientState.Quit;
            }
        });
        this.socket.addEventListener("message", (event) => {
            let newdata = this.hanging_data + event.data;
            let lines = newdata.split("\r\n");
            this.hanging_data = lines.pop();
            lines.forEach((data2) => {
                if (data2 != "") {
                    this.resetWatchdog();
                    let msg = this.processMessage(data2);
                    if (msg.command === "PING") {
                        this.socket.send(data2.replace("PING", "PONG") + '\r\n');
                    }
                    if (msg.command === "422" || msg.command === "376") {
                        this.status = exports.ClientState.Ready;
                        this.options.postConnectCommands.forEach((cmd) => {
                            this.sendData(cmd);
                        });
                        this.channels.forEach((channel) => {
                            this.sendData("JOIN " + channel.name);
                        });
                    }
                    if (msg.command === "JOIN") {
                        let chn_name = msg.parameters[0];
                        let chn = this.findChannel(chn_name);
                        if (chn === null) {
                            chn = new Channel(chn_name);
                            this.channels.push(chn);
                        }
                        chn.joinCallback();
                    }
                    this.emit('message', msg);
                }
            });
        });
        this.socket.addEventListener("error", (error) => {
            console.log("ERROR", error);
        });
    }
    _disconnect() {
        try {
            this.socket.close();
        }
        catch (err) {
            console.error(err);
        }
    }
    reconnect() {
        this.disconnect();
        setTimeout(this.connect.bind(this), 3000);
    }
    sendData(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(data + "\r\n");
    }
    sendDataRaw(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.send(Buffer.concat([data, Buffer.from("\r\n")]));
    }
}
class TwitchClientWS extends WebSocketClient {
    constructor(options) {
        const newOptions = Object.assign(Object.assign({}, options), { "mode": "ws", "url": "wss://irc-ws.chat.twitch.tv:443/" });
        newOptions.preConnectCommands = [...options.preConnectCommands || [],
            "CAP REQ :twitch.tv/tags",
            "CAP REQ :twitch.tv/commands",
            "CAP REQ :twitch.tv/membership"
        ];
        if (!options.nickname || !options.oauth) {
            newOptions.nickname = 'justinfan' + (Math.floor(Math.random() * (900000)) + 100000).toString();
        }
        else {
            newOptions.preConnectCommands.push("PASS " + options.oauth);
        }
        super(newOptions);
        this.on('message', this.twitch_join_handler.bind(this));
    }
    twitch_join_handler(msg) {
        if (msg.command === "ROOMSTATE") {
            let roomname = "#" + msg.parameters[0];
            this.channels.forEach((channel) => {
                if (channel.name === roomname) {
                    channel.join_state = true;
                }
            });
        }
        else if (msg.command === "RECONNECT") {
            setTimeout(this.reconnect.bind(this), 2000);
        }
    }
    processMessage(data) {
        return new TwitchMessage(data);
    }
}

class SocketClient extends BaseClient {
    constructor(options) {
        super();
        this.options = options;
        this.options.preConnectCommands = options.preConnectCommands || [];
        this.options.postConnectCommands = options.postConnectCommands || [];
        this.channels = [];
        this.status = exports.ClientState.Disconnected;
        this.hanging_data = "";
    }
    _connect_stage2() {
        this.initWatchdog();
        this.socket.setEncoding("utf8");
        this.socket.setNoDelay(true);
        this.socket.on("close", () => {
            if (this.status != exports.ClientState.Quiting) {
                this.status = exports.ClientState.Disconnected;
                this.channels.forEach((channel) => {
                    channel.join_state = false;
                });
                setTimeout(this.connect.bind(this), 5000);
            }
            else {
                this.status = exports.ClientState.Quit;
            }
        });
        this.socket.on("data", (data) => {
            let newdata = this.hanging_data + data;
            let lines = newdata.split("\r\n");
            this.hanging_data = lines.pop();
            lines.forEach((data2) => {
                if (data2 != "") {
                    this.resetWatchdog();
                    let msg = this.processMessage(data2);
                    if (msg.command === "PING") {
                        this.socket.write(data2.replace("PING", "PONG") + '\r\n');
                    }
                    if (msg.command === "422" || msg.command === "376") {
                        this.status = exports.ClientState.Ready;
                        this.options.postConnectCommands.forEach((cmd) => {
                            this.sendData(cmd);
                        });
                        this.channels.forEach((channel) => {
                            this.sendData("JOIN " + channel.name);
                        });
                    }
                    if (msg.command === "JOIN") {
                        let chn_name = msg.parameters[0];
                        let chn = this.findChannel(chn_name);
                        if (chn === null) {
                            chn = new Channel(chn_name);
                            this.channels.push(chn);
                        }
                        chn.joinCallback();
                    }
                    this.emit('message', msg);
                }
            });
        });
        this.socket.on("error", (error) => {
            console.log("ERROR", error);
        });
    }
    sendData(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.write(data + "\r\n");
    }
    sendDataRaw(data, log = true) {
        if (log) {
            if (data.includes("PASS ") || data.includes("OPER ")) {
                console.log("> **********");
            }
            else {
                console.log(">", data);
            }
        }
        this.socket.write(Buffer.concat([data, Buffer.from("\r\n")]));
    }
    reconnect() {
        this.disconnect();
        setTimeout(this.connect.bind(this), 3000);
    }
    _disconnect() {
        try {
            this.socket.end();
            this.socket.destroy();
        }
        catch (err) {
            console.error(err);
        }
    }
}
class SSLClient extends SocketClient {
    constructor(options) {
        super(options);
    }
    _connect_callback() {
        if (!this.socket.authorized) {
            this.reconnect();
        }
        else {
            console.log("Port:", this.socket.remotePort);
            console.log("Host:", this.socket.remoteAddress);
            console.log("Connection Authorized:", this.socket.authorized);
            this.status = exports.ClientState.Connected;
            this.options.preConnectCommands.forEach((cmd) => {
                this.sendData(cmd);
            });
            this.sendData("NICK " + this.options.nickname);
            this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
        }
    }
    connect() {
        if (this.status === exports.ClientState.Connected) {
            return;
        }
        else {
            let options = {
                host: this.options.host,
                port: parseInt(this.options.port),
                servername: this.options.host
            };
            this.socket = TLS.connect(options);
            this.socket.on("secureConnect", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
}
class NETClient extends SocketClient {
    constructor(options) {
        super(options);
    }
    _connect_callback() {
        console.log("Port:", this.socket.remotePort);
        console.log("Host:", this.socket.remoteAddress);
        this.status = exports.ClientState.Connected;
        this.options.preConnectCommands.forEach((cmd) => {
            this.sendData(cmd);
        });
        this.sendData("NICK " + this.options.nickname);
        this.sendData("USER " + this.options.nickname + " * * :" + this.options.nickname);
    }
    connect() {
        if (this.status === exports.ClientState.Connected) {
            return;
        }
        else {
            let options = {
                host: this.options.host,
                port: parseInt(this.options.port),
                servername: this.options.host
            };
            this.socket = NET.connect(options);
            this.socket.on("connect", this._connect_callback.bind(this));
            this._connect_stage2();
        }
    }
}
class TwitchClient extends SSLClient {
    constructor(options) {
        const newOptions = Object.assign(Object.assign({}, options), { "mode": "tls", "host": "irc.chat.twitch.tv", "port": "6697" });
        newOptions.preConnectCommands = [...options.preConnectCommands || [],
            "CAP REQ :twitch.tv/tags",
            "CAP REQ :twitch.tv/commands",
            "CAP REQ :twitch.tv/membership"
        ];
        if (!options.nickname || !options.oauth) {
            newOptions.nickname = 'justinfan' + (Math.floor(Math.random() * (900000)) + 100000).toString();
        }
        else {
            newOptions.preConnectCommands.push("PASS " + options.oauth);
        }
        super(newOptions);
        this.on('message', this.twitch_join_handler.bind(this));
    }
    twitch_join_handler(msg) {
        if (msg.command === "ROOMSTATE") {
            let roomname = "#" + msg.parameters[0];
            this.channels.forEach((channel) => {
                if (channel.name === roomname) {
                    channel.join_state = true;
                }
            });
        }
        else if (msg.command === "RECONNECT") {
            setTimeout(this.reconnect.bind(this), 2000);
        }
    }
    processMessage(data) {
        return new TwitchMessage(data);
    }
}

class Client {
    constructor(options) {
        switch (options.mode || 'tls') {
            case ('tls'): {
                return new SSLClient(options);
            }
            case ('net'): {
                return new NETClient(options);
            }
            case ('ws'): {
                return new WebSocketClient(options);
            }
        }
    }
    connect() { }
    ;
    joinChannel(channel) { return false; }
    ;
    sendData(data) { }
    ;
    sendDataRaw(data) { }
    ;
    findChannel(channel) { return new Channel(''); }
    ;
    leaveChannel(channel) { }
    ;
    listChannels() { }
    ;
    disconnect() { }
    ;
    reconnect() { }
    ;
    on(eventName, listener) { return new events.EventEmitter; }
    ;
    off(eventName, listener) { return new events.EventEmitter; }
    ;
    once(eventName, listener) { return new events.EventEmitter; }
    ;
}

exports.Channel = Channel;
exports.Client = Client;
exports.Message = Message;
exports.Prefix = Prefix;
exports.TwitchClient = TwitchClient;
exports.TwitchClientWS = TwitchClientWS;
exports.TwitchMessage = TwitchMessage;
