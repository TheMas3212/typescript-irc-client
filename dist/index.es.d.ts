/// <reference types="node" />
import { EventEmitter } from 'events';
import { Channel } from './Objects';
import { ClientInterface } from './BaseClient';
export * from './Objects';
export { TwitchClientWS } from './WebSocketClientB';
export { ClientState } from './BaseClient';
export declare type ClientMode = 'ws';
export declare type WebSocketOptions = {
    "mode": "ws";
    "url": string;
};
export declare type SharedOptions = {
    "nickname": string;
    "logging"?: boolean;
    "preConnectCommands"?: string[];
    "postConnectCommands"?: string[];
};
export declare type ClientOptions = (WebSocketOptions) & SharedOptions;
export declare class Client implements ClientInterface {
    options: ClientOptions;
    channels: Channel[];
    status: number;
    hanging_data: string;
    socket: WebSocket;
    constructor(options: ClientOptions);
    connect(): void;
    joinChannel(channel: string): boolean;
    sendData(data: string, log?: boolean): void;
    sendDataRaw(data: Buffer, log?: boolean): void;
    findChannel(channel: string): Channel;
    leaveChannel(channel: string): void;
    listChannels(): void;
    disconnect(): void;
    reconnect(): void;
    on(eventName: string, listener: Function): EventEmitter;
    off(eventName: string, listener: Function): EventEmitter;
    once(eventName: string, listener: Function): EventEmitter;
}
